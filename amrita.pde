import java.io.*;
import processing.video.*;
import SimpleOpenNI.*;
import processing.video.*;
import java.util.List;
import java.util.ArrayList;
import java.awt.*;
import ddf.minim.*;
import ddf.minim.ugens.*;

// ******************* PHASE 1 PARAMETERS *****************************
// Settings
private final int WIDTH = 800; // 640
private final int HEIGHT = 1280; // 896
private final float CURSOR_Y_SCALE = 1.3;
private final float CURSOR_X_SCALE = 1.3;
private final int MIN_FRAMERATE = 20;
private final int FRAMERATE = 30;
private final boolean ENABLEFULLSCREEN = false;

private char showScreen = '1';
private final char SCREEN_MAIN = '1';
private final char SCREEN_OPTIONS = '2';
private final char SCREEN_DISTANCE = '3';
private final char SCREEN_DEPTH = '4';
private final char SCREEN_DEPTH_PLANES = '5';
private final char SCREEN_CAM = '6';
private boolean drawBox = true;
private boolean drawCursor = true;

private int[] tuioPlanes = new int[3];
private final int TUIO_MIN_PLANE = 1000;
private final int TUIO_MAX_PLANE = 1500;
private int[] phasePlanes = new int[2];
private final int PHASE_MIN_PLANE = 1000;
private final int PHASE_MAX_PLANE = 1500;
private boolean showTuioCaliPlane = false;

private int[] tuioBox = new int[4];  // {x1,y1,x2,y2}
private final int[] tBoxColor = {255,255,0};
private int[] phaseBox = new int[4];
private final int[] pBoxColor = {0,255,0};
private boolean drawPhaseBox = true;

private boolean calibrating = false;
private int cursorXorigin = 0;
private int cursorYorigin = 0;
private ArrayList<PVector> calibCursors;
private ArrayList<PVector> smoothingCursors;
private int cursorX = 0;
private int cursorY = 0;


private final int PIXEL_FOR_CHANGE = 30;
private final long TIME_TILL_CHANGE = 4000; // in millisekunden
private long oldWaitForChange;

private boolean mPressed = false;

private final int KWIDTH = 640;
private final int KHEIGHT = 480;

private final int DAMP = 5;
private final int DIST_THRESH = 250;//250; // threshold in mm
private final int DIST_AMP = 128; // disturbance amplitude
private final int MAX_AMP = 1536;
private final int DIST_TRIGGER = 0;//500;
private final int DIST_DISCARD_DISTURB = 1800;
private final long disTime = 10;//150;

private long momTime = 0;
private long oldTime;
private long timeSinceLastFrame;
private int fps = 0;
private long timeTmp;
private long frameTime;
private long deltaT = 0;
private boolean dropFrame = false;
private int frameDrops = 0;

private boolean showDifMap = false;

// Variables
private float v = 1.0 / 9.0;
private final int MEDIAN = 0;
private float[][] medianKernel = {
      {v,v,v},
      {v,v,v},
      {v,v,v}};

private PImage img;
private Movie mWaterMovie;
private Movie mTransitionMovie;
private Movie mReverseTransitionMovie;
private SimpleOpenNI kinect;
private int[] dmapBuffer;

private int[][] bufSrc;
private int[][] bufDest;

private PImage mInfo1;
private PImage mInfo2;
private float mInfoAlpha1;
private float mInfoAlpha2;
private boolean mShowInfo;
private boolean mAllInfoShown;
private int mInfoFramesShown;
private long mMaximumCalibTime;
// ******************* PHASE 2 PARAMETERS *****************************

static int CALIB_THRESHOLD = 10;
static int SMOOTHING_CURSOR_SIZE = 15;

// ******************* Audio PARAMETERS *****************************
private Minim minim;
private AudioOutput out;
private long startTime;
private Sampler[] channels;
private boolean[] channelFade;
private float[]   fadeSpeed;
private float FADE_CONSTANT = 1f; 


// ******* Speed parameters
static float MAX_SPEED = 20;
static float AVG_SPEED = 5;
static float MIN_SPEED = 3;

static float COLOR_CHANGE_SPEED = 0.002f; // 0.005

static int VORTEX_SIZE = 6;
static int VORTEX_DIST = 400;
static int VORTEX_LENGTH = (VORTEX_SIZE-1)*VORTEX_DIST;

static float BG_FACTOR =  255.0 / (16.0*VORTEX_LENGTH);

// ******* Vortex parameters
class Phase
{
  public float flowLength;
  public int vortexLayerCount;
  public float vortexRadius;
  public float[] vortexRadiusFactors;
  public String[] vortexTextures;
  
  public boolean amoeba;
  public boolean jellyfish;
  public boolean moreJellyfish;
  public boolean strobo;
  public boolean sudden;
  public boolean bubbles;
}

int mCurrentPhase;
Phase[] mPhases;
void initPhases()
{
  mPhases = new Phase[5];
  mPhases[0] = new Phase();
  mPhases[1] = new Phase();
  mPhases[2] = new Phase();
  mPhases[3] = new Phase();
  mPhases[4] = new Phase();
  
  mPhases[0].flowLength = 3*VORTEX_LENGTH;
  mPhases[0].vortexRadius = 40;
  mPhases[0].vortexLayerCount = 1;
  mPhases[0].vortexRadiusFactors = new float[1];
  mPhases[0].vortexTextures = new String[1];
  mPhases[0].vortexRadiusFactors[0] = 1;
  mPhases[0].vortexTextures[0] = "Phase2/Texturen_Strudel/Wasser.png";
  
  mPhases[0].amoeba = false;
  mPhases[0].jellyfish = false;
  mPhases[0].moreJellyfish = false;
  mPhases[0].strobo = false;
  mPhases[0].sudden = false;
  mPhases[0].bubbles = true;
  
  mPhases[1].flowLength = 3*VORTEX_LENGTH;
  mPhases[1].vortexRadius = 40;
  mPhases[1].vortexLayerCount = 1;
  mPhases[1].vortexRadiusFactors = new float[1];
  mPhases[1].vortexTextures = new String[1];
  mPhases[1].vortexRadiusFactors[0] = 1;
  mPhases[1].vortexTextures[0] = "Phase2/Texturen_Strudel/Wasser.png";
  
  mPhases[1].amoeba = false;
  mPhases[1].jellyfish = true;
  mPhases[1].moreJellyfish = false;
  mPhases[1].strobo = false;
  mPhases[1].sudden = false;
  mPhases[1].bubbles = true;

  mPhases[2].flowLength = 3*VORTEX_LENGTH;
  mPhases[2].vortexRadius = 40;
  mPhases[2].vortexLayerCount = 2;
  mPhases[2].vortexRadiusFactors = new float[2];
  mPhases[2].vortexTextures = new String[2];
  mPhases[2].vortexRadiusFactors[0] = 1;
  mPhases[2].vortexTextures[0] = "Phase2/Texturen_Strudel/Wasser.png";
  mPhases[2].vortexRadiusFactors[1] = 1.3;
  mPhases[2].vortexTextures[1] = "Phase2/Texturen_Strudel/DNA_diagonal.png";
  
  mPhases[2].amoeba = false;
  mPhases[2].jellyfish = false;
  mPhases[2].moreJellyfish = true;
  mPhases[2].strobo = false;
  mPhases[2].sudden = false;
  mPhases[2].bubbles = true;

  mPhases[3].flowLength = 3*VORTEX_LENGTH;
  mPhases[3].vortexRadius = 40;
  mPhases[3].vortexLayerCount = 2;
  mPhases[3].vortexRadiusFactors = new float[2];
  mPhases[3].vortexTextures = new String[2];
  mPhases[3].vortexRadiusFactors[0] = 1;
  mPhases[3].vortexTextures[0] = "Phase2/Texturen_Strudel/DNA_vertikal.png";
  mPhases[3].vortexRadiusFactors[1] = 1.3;
  mPhases[3].vortexTextures[1] = "Phase2/Texturen_Strudel/DNA_diagonal.png";
  
  mPhases[3].amoeba = true;
  mPhases[3].jellyfish = false;
  mPhases[3].moreJellyfish = false;
  mPhases[3].strobo = false;
  mPhases[3].sudden = false;
  mPhases[3].bubbles = true;
  
  mPhases[4].flowLength = 3*VORTEX_LENGTH;
  mPhases[4].vortexRadius = 40;
  mPhases[4].vortexLayerCount = 2;
  mPhases[4].vortexRadiusFactors = new float[2];
  mPhases[4].vortexTextures = new String[2];
  mPhases[4].vortexRadiusFactors[0] = 1;
  mPhases[4].vortexTextures[0] = "Phase2/Texturen_Strudel/DNA_vertikal_2Color.png";
  mPhases[4].vortexRadiusFactors[1] = 1.3;
  mPhases[4].vortexTextures[1] = "Phase2/Texturen_Strudel/DNA_diag_2Color.png";
  
  mPhases[4].amoeba = false;
  mPhases[4].jellyfish = false;
  mPhases[4].moreJellyfish = false;
  mPhases[4].strobo = true;
  mPhases[4].sudden = true;
  mPhases[4].bubbles = true;
  
}

static int FLOATY = 0;
static int CIRCULAR = 1;
static int BUBBLE = 2;

PImage mTargetImage;
ParticleSystem mAmoeba;
ParticleSystem mJellyfish;
ParticleSystem mMoreJellyfish;
ParticleSystem mStrobo;
ParticleSystem mSudden;
ParticleSystem mBubbles;
void initParticleSystems()
{
  mTargetImage = loadImage("Phase2/target.png");
  mAmoeba = new ParticleSystem(100, "Phase2/Objekte_Sprites/Amobe_20f/Amobe_", 20, 0.5, true, FLOATY, 15);
  mJellyfish = new ParticleSystem(20, "Phase2/Objekte_Sprites/Qualle_20f/Qualle_", 20, 0.5, true, FLOATY, 30);
  mMoreJellyfish = new ParticleSystem(50, "Phase2/Objekte_Sprites/Qualle_20f/Qualle_", 20, 0.5, true, FLOATY, 30);
  mStrobo = new ParticleSystem(10, "Phase2/Objekte_Sprites/Strobo_20f/Strobo_", 20, 0.5, true, CIRCULAR, 30);
  mSudden = new ParticleSystem(10, "Phase2/Objekte_Sprites/Sudden_20f/Sudden_", 20, 0.5, true, CIRCULAR, 30);
  mBubbles = new ParticleSystem(200, "Phase2/Objekte_Sprites/Blubber_", 1, 0.5, false, BUBBLE, 2);
}

Vortex mVortex;

void switchPhase(int nextPhase)
{
  System.out.println("switch to " + String.valueOf(nextPhase));
  Phase p = mPhases[nextPhase];
  
  if(p.amoeba) mAmoeba.activate();
  else mAmoeba.deactivate();  
  
  if(p.jellyfish) mJellyfish.activate();
  else mJellyfish.deactivate();  
  
  if(p.moreJellyfish) mMoreJellyfish.activate();
  else mMoreJellyfish.deactivate();  
  
  if(p.strobo) mStrobo.activate();
  else mStrobo.deactivate();  
  
  if(p.sudden) mSudden.activate();
  else mSudden.deactivate();  
  
  if(p.bubbles) mBubbles.activate();
  else mBubbles.deactivate();
  
  mVortex.setParams(p);
}

// path will be chosen randomly inside [-VORTEX_WIDTH/2, VORTEX_WIDTH/2]x[-VORTEX_HEIGHT/2,VORTEX_HEIGHT/2]
// * set in in setup function 
int VORTEX_WIDTH;        
int VORTEX_HEIGHT;


// ******* Background movie
static String BG_MOVIE_PATH = "Phase2/Movies/Game_BG.mp4";
static String TRANS_MOVIE_PATH = "transition_bg.mp4";
static String REVERSE_TRANS_MOVIE_PATH = "transition_bg_re.mp4";
// ******************************************************************


Movie mBgMovie;

PShader postprocess;
PShader bgGradient;
PShader texLight; 
PShader light; 
float mZMod;
float mFlow;
float mFlowAccu;
float mFlowPhaseAccu;
float mDepth;
float mA;
PVector mTarget;
PVector mCameraPosition;
int mSuperPhase;
boolean changePhase;

boolean movieFrameReady;

void setup()
{
  size(WIDTH, HEIGHT, P3D);
  mSuperPhase = 1;
  changePhase = false;
  loadMovies();
  frameRate(FRAMERATE);

  // init kinect
  kinect = new SimpleOpenNI(this);
  kinect.setMirror(true);
  kinect.enableDepth();
  kinect.enableRGB();

  // init buffers for wave calculation (extra borders filled with 0s)
  bufSrc = new int[WIDTH+2][HEIGHT+2];
  bufDest = new int[WIDTH+2][HEIGHT+2];

  // init buffer for depth map
  dmapBuffer = new int[KHEIGHT*KWIDTH];
  
  tuioPlanes[0] = TUIO_MIN_PLANE;
  tuioPlanes[1] = TUIO_MAX_PLANE;
  tuioPlanes[2] = TUIO_MAX_PLANE;
  phasePlanes[0] = PHASE_MIN_PLANE;
  phasePlanes[1] = PHASE_MAX_PLANE;
  //init sound
  minim = new Minim(this);
  out   = minim.getLineOut();
  channels = new Sampler[6];
  channelFade = new boolean[6];
  fadeSpeed = new float[6];

  for(int i=0; i<channels.length; i++) {
    channels[i] = new Sampler( "audio/spur"+(i+1)+".wav", 2, minim );
    channels[i].patch(out);
    channels[i].looping = true;
  }
  // start all channels
  for(Sampler s: channels) {
    s.amplitude.setLastValue(0f);  // mute channel
    s.trigger();
  }
  
  enterPhase1();
}

void loadMovies()
{
  mInfo1 = loadImage("instruction_calibrate.png");
  mInfo2 = loadImage("instruction_control.png");

  mWaterMovie = new Movie(this, sketchPath("Phase1/Hintergrund_MASTER.mp4"));
  mBgMovie = new Movie(this, sketchPath(BG_MOVIE_PATH));
  mTransitionMovie = new Movie(this, sketchPath(TRANS_MOVIE_PATH));
  mReverseTransitionMovie = new Movie(this, sketchPath(REVERSE_TRANS_MOVIE_PATH));
  
  texLight = loadShader("myTexFrag.glsl", "myTexlightVert.glsl");
  texLight.set("fogNear", 0.0); 
  texLight.set("fogFar", 3000.0);
  
  light = loadShader("myLightFrag.glsl", "myLightVert.glsl");
  light.set("fogNear", 0.0); 
  light.set("fogFar", 3000.0);
  
  bgGradient = loadShader("bgGradientFrag.glsl", "bgGradientVert.glsl");
  bgGradient.set("resolution", (float)WIDTH, (float)HEIGHT);
  
  postprocess = loadShader("postprocessColor.glsl");
}

float lerpDegrees(float start, float end, float t)
{
  	float difference = abs(end - start);
  	if (difference > 180)
  	{
    		// We need to add on to one of the values.
    		if (end > start)
    		{
    			// We'll add it on to start...
    			start += 360;
    		}
    		else
    		{
    			// Add it on to end.
    			end += 360;
    		}
  	}
        
    if(difference < t)
    {
      t = difference;
    }

	// Interpolate it.
	float value = start + t;

	// Wrap it..
	return ((value+360) % 360);
}

float mCurrentHValue = 0;
float mTargetHValue = 0;
void colorFade(float delta)
{
  if(mCurrentHValue == mTargetHValue)
  {
    mTargetHValue = random(360);
  }
  
  mCurrentHValue = lerpDegrees(mCurrentHValue, mTargetHValue, delta);
  postprocess.set("hValue", mCurrentHValue, 0, 0);
  filter(postprocess);
  /*
  loadPixels();
  
  for(int y = 0; y < HEIGHT; ++y)
  {
    for(int x = 0; x < WIDTH; ++x)
	{
	  int idx = y*WIDTH + x;
	  color p = pixels[idx];
	  float[] hsb = Color.RGBtoHSB((p>>16)&0xFF, (p>>8)&0xFF, p&0xFF, null);
	  hsb[0] += mCurrentHValue;
	  hsb[0] %= 360;
	  pixels[idx] = Color.HSBtoRGB(hsb[0], hsb[1], hsb[2]);
	}
  }
  updatePixels();
  */
}

void draw()
{
  tint(255);
  if(showScreen == SCREEN_MAIN)
  {
    boolean activityDetected = countPixels();
    if(mSuperPhase == 1)
    {
      // we are in game if no activity for long time switch to phase 1
      if(activityDetected)
      {
        if(System.currentTimeMillis()-oldWaitForChange >= TIME_TILL_CHANGE)
        {
          mSuperPhase = 3;
          System.out.println("phase 3");
          enterPhase3();
        }
      }
      else oldWaitForChange = System.currentTimeMillis();
    }
    else if(mSuperPhase == 2)
    {
      // we are in game if no activity for long time switch to phase 1
      if(!activityDetected && !calibrating)
      {
        if(System.currentTimeMillis()-oldWaitForChange >= TIME_TILL_CHANGE)
        {
          mSuperPhase = 4;
          System.out.println("phase 4");
          enterPhase4();
        }
      }
      else oldWaitForChange = System.currentTimeMillis();
    }
  }
  
  if (mSuperPhase == 1 || mSuperPhase == 3 || mSuperPhase == 5)       drawPhase1();
  if (mSuperPhase == 2 || mSuperPhase == 4) drawPhase2();
  if (mSuperPhase == 3) drawPhase3();
  if (mSuperPhase == 4) drawPhase4();
  if (mSuperPhase == 5) drawPhase5();
}

int transitionAlpha = 0;
long transitionStopTime = 0;
void enterPhase3()
{
  mInfoAlpha1 = 0;
  mInfoAlpha2 = 0;
  mShowInfo = true;
  mAllInfoShown = false;
  mInfoFramesShown = 0;
  transitionAlpha = 0;
  transitionStopTime = System.currentTimeMillis() + 6800;
  mTransitionMovie.play();
  movieFrameReady = false;
}
void drawPhase3()
{
  transitionAlpha += 12;
  transitionAlpha = min(255, transitionAlpha);
  tint(255, transitionAlpha);
  if(movieFrameReady);
    image(mTransitionMovie, 0, 0, WIDTH, HEIGHT);
  
  if(System.currentTimeMillis() > transitionStopTime - 2000)
  {
    mInfoAlpha1 += 20;
    mInfoAlpha1 = min(255, mInfoAlpha1);
    tint(255, mInfoAlpha1);
    image(mInfo1, width/2-227, height/2-207);
  }
  
  
  if(System.currentTimeMillis() > transitionStopTime)
  {
    exitPhase1();
    exitPhase3();
    mSuperPhase = 2;
    enterPhase2();
  }
}
void exitPhase3()
{
  tint(255);
  mTransitionMovie.stop();
}

void enterPhase4()
{
  transitionAlpha = 0;
  transitionStopTime = System.currentTimeMillis() + 6800;
  mReverseTransitionMovie.play();
  movieFrameReady = false;
}
void drawPhase4()
{
  transitionAlpha += 12;
  transitionAlpha = min(255, transitionAlpha);
  tint(255, transitionAlpha);
  if(movieFrameReady);
    image(mReverseTransitionMovie, 0, 0, WIDTH, HEIGHT);
  
  if(System.currentTimeMillis() > transitionStopTime - 1500)
  {
    exitPhase2();
    exitPhase4();
    mSuperPhase = 5;
    enterPhase1();
    enterPhase5();
  }
}
void exitPhase4()
{
  mReverseTransitionMovie.stop();
}
void enterPhase5()
{
  transitionAlpha = 255;
  
  // sound
  for (int i=0; i<channels.length; ++i)
      setChannelFade(i, false, 1.f);
}
void drawPhase5()
{
  transitionAlpha -= 10;
  transitionAlpha = max(0, transitionAlpha);
  System.out.println(transitionAlpha);
  tint(255, transitionAlpha);
  if(movieFrameReady);
    image(mReverseTransitionMovie, 0, 0, WIDTH,HEIGHT);
  
  
  long momTime = System.currentTimeMillis();
  fadeChannels(momTime-startTime);
  startTime = momTime;
  
  if(System.currentTimeMillis() > transitionStopTime)
  {
    exitPhase5();
    mSuperPhase = 1;
  }
}
void exitPhase5()
{
  tint(255);
  mReverseTransitionMovie.stop();
  stopSound();
}
// ***************** PHASE 1 MEMBERS ******************
void enterPhase1()
{
  
  strokeWeight(1);
  rectMode(CORNERS);
  
  mWaterMovie.loop();
  movieFrameReady = false;
  //smooth();
  oldWaitForChange = System.currentTimeMillis();
  oldTime = System.currentTimeMillis();
  timeSinceLastFrame = System.currentTimeMillis();
}
void exitPhase1()
{
  mWaterMovie.stop();
}


void drawPhase1()
{
    momTime = System.currentTimeMillis();
    
    frameTime = momTime-oldTime;
    
    if(deltaT >= 1000) {
      deltaT -= 1000;
      //System.out.println("fps: "+fps+"; timeSinceLastFrame: "+timeSinceLastFrame+"; frameTime:"+frameTime+"; dropped Frames: "+frameDrops);
      fps = 1;
      frameDrops = 0;
    }
    else {
      deltaT += frameTime;
      fps++;
    }
    
    if(!dropFrame && (1000/MIN_FRAMERATE)*fps < deltaT) {
      dropFrame = true;
      frameDrops++;
    }
    else {
      dropFrame = false;
    }
    
    if(frameTime > disTime) {
      oldTime = momTime;
      timeTmp = System.currentTimeMillis();
      compareAndDisturb(getDmap());
    }
    
    if(!dropFrame && (showScreen == SCREEN_MAIN || showScreen == SCREEN_DISTANCE)) {
      // draw the frame
      renderFrame(System.currentTimeMillis() - timeSinceLastFrame);
      timeSinceLastFrame = System.currentTimeMillis();
    }
    switch(showScreen) {
      case SCREEN_MAIN: 
          // calc wave movement
          processWater();
          break;
      case SCREEN_DEPTH_PLANES:
          kinect.update();
          // draw depthImage between the min- and max-plane
          renderWithPlanes(208);
          drawOptionUI();
          break;
      case SCREEN_DEPTH:
          kinect.update();
          background(0, 0, 0);
          
          // draw depthImageMap
          image(kinect.depthImage(), 0, 208);
          drawOptionUI();
          break;
      case SCREEN_CAM:
          kinect.update();
          // draw depthImageMap
          image(kinect.rgbImage(), 0, 208);
          drawOptionUI();
          break;
    }
    
}

private void drawOptionUI() {
  if(drawBox) {
    // draw rectangle
    noFill();
    stroke(tBoxColor[0],tBoxColor[1],tBoxColor[2]);
    rect(tuioBox[0], tuioBox[1]+208, tuioBox[2], tuioBox[3]+208);
    stroke(pBoxColor[0],pBoxColor[1],pBoxColor[2]);
    rect(phaseBox[0], phaseBox[1]+208, phaseBox[2], phaseBox[3]+208);
  }
  if(drawCursor) {
    stroke(255,0,0);
    fill(255,0,0);
    rect(cursorX-3, cursorY-3, cursorX+3, cursorY+3);
  }
  textSize(16);
    stroke(255,255,255);
  //header
  if(drawPhaseBox)
    text("Draw Box: Phase-Change Box", 20, 20);
  else
    text("Draw Box: TUIO Box", 20, 20);
  text("Vorzeichen: "+sign, 20, 38);
  
  //footer
  text("Planes:            min     max", 20, 700);
  text("phase-change  "+phasePlanes[0]+"   "+phasePlanes[1], 20, 718);
  text("TUIO               "+tuioPlanes[0]+"   "+tuioPlanes[1]+"   "+tuioPlanes[2], 20, 736);
  
}

private int[] getDmap() {
  int[] dmap;

  // get depth map
  kinect.update();
  dmap = kinect.depthMap();

  return dmap.clone();
}

public void renderWithPlanes(int line) {
  int[] tmp;
  if(drawPhaseBox)
    tmp = phasePlanes;
  else
    tmp = tuioPlanes;
    
  int[] depMap = getDmap();
  
  background(0, 0, 0);
  loadPixels();
  
  int maxPlane = tmp[1];
  
  if(showTuioCaliPlane  && !drawPhaseBox)
    maxPlane = tmp[2];
    
  for (int ky=0; ky<KHEIGHT; ky++) {
    for (int kx=0; kx<KWIDTH; kx++) {
      int pos = depMap[ky * KWIDTH + kx];
      if(pos >= tmp[0] && pos <= maxPlane) {
        if(drawPhaseBox)
          pixels[WIDTH*(line+ky)+kx] = color(255, 204, 0);
        else 
          pixels[WIDTH*(line+ky)+kx] = color(255, 204, 0);
      }
      else {
        pixels[WIDTH*(line+ky)+kx] = 0;
      }
    }
  }
  calcCursorPos(pixels, 208);
  updatePixels();
}

private boolean countPixels() {
  int[] depMap = kinect.depthMap();
  int sum = 0;
  for(int x = phaseBox[0]; x<phaseBox[2]; x++) {
    for(int y = phaseBox[1]; y<phaseBox[3]; y++) {
      int pos = depMap[y*KWIDTH+x];
      if(pos >= phasePlanes[0] && pos <= phasePlanes[1])
        sum++;
    }
  }
  if(sum >= PIXEL_FOR_CHANGE) {
    return true;
  }
  else {
    return false;
  }
}

private boolean calcCursorPos(int[] depMap, int offset) {
  int smallest = 100000;
  int pos;
  int tmp = 1;
  int sum = 0;
  if(calibrating)
    tmp = 2;
  
  for(int x = tuioBox[0]; x<tuioBox[2]; x++) {
    for(int y = tuioBox[1]+offset; y<tuioBox[3]+offset; y++) {
      pos = depMap[y*KWIDTH+x];
      if(pos > tuioPlanes[0] && pos < tuioPlanes[tmp]) {
        sum++;
        if(pos < smallest)
        {
          smallest = pos;
          cursorX = x;
          cursorY = y;
        }
      }
    }
  }
  if(sum >= PIXEL_FOR_CHANGE) {
    return true;
  }
  else {
    return false;
  }
}

private void compareAndDisturb(int[] newDmap) {
  int[] diffMap = new int[KWIDTH*KHEIGHT];

  // compare pixels, generate diffmap
  for (int ky=0; ky<KHEIGHT; ky++) {
    for (int kx=0; kx<KWIDTH; kx++) {
      int pos = ky * KWIDTH + kx;
      // if distance change is not 0 (to avoid noise)
      if ((dmapBuffer[pos]!=0)&&(newDmap[pos]!=0)) {
        int diff = Math.abs(dmapBuffer[pos]-newDmap[pos]);
        // if distance change is big enough
        if (diff > DIST_THRESH)
          diffMap[pos] =  diff;
        else
          diffMap[pos] = 0;
      }
    }
  }

  // set dmapBuffer to new frame
  dmapBuffer = newDmap;

  // use filter
  kFilterWithThreshold(diffMap, KWIDTH, KHEIGHT, MEDIAN, DIST_DISCARD_DISTURB);
  
  // check again if distance change is still big enough
  for (int ky=0; ky<KHEIGHT; ky++) {
    for (int kx=0; kx<KWIDTH; kx++) {
      int pos = ky * KWIDTH + kx;
      if (diffMap[pos] > DIST_THRESH)
        // disturb according pixels
        disturb(kx, ky, DIST_AMP);
    }
  }

}

// translate disturbance from kinect at kx,xy to buffer pixels
private void disturb(int kx, int ky, int amplitude) {
  // ignore border
  if (kx==0) return;
  if (ky==0) return;
  if (kx==KWIDTH-1) return;
  if (ky==KHEIGHT-1) return;

  final double ratio_x = WIDTH/KWIDTH;
  final double ratio_y = HEIGHT/KHEIGHT;
  // 1+ because of border in array
  final int x_min = 1+(int)(kx * ratio_x);
  final int y_min = 1+(int)(ky * ratio_y);
  final int x_max = 1+(int)((kx+1) * ratio_x);
  final int y_max = 1+(int)((ky+1) * ratio_y);

  // disturb pixels in buffer that are assigned to the dmap pixel
  for (int x=x_min; x<x_max; ++x)
    for (int y=y_min; y<y_max; ++y)
      bufSrc[x][y] = amplitude;
}

private void processWater() {
  // see http://freespace.virgin.net/hugo.elias/graphics/x_water.htm
  for (int x=1; x<WIDTH+1; ++x) {
    for (int y=1;y<HEIGHT+1; ++y) {
      bufDest[x][y] = ((
          bufSrc[x-1][y] +
          bufSrc[x+1][y] +
          bufSrc[x][y-1] +
          bufSrc[x][y+1]
        ) >> 1) - bufDest[x][y];
      bufDest[x][y] -= bufDest[x][y] >> DAMP;
      if(bufDest[x][y] > MAX_AMP)
        bufDest[x][y] = MAX_AMP;
    }
  }

  // swap references
  int tmp[][] = bufDest;
  bufDest = bufSrc;
  bufSrc = tmp;
}

private void renderFrame(long milSec) {
  
  int[] oldPix;
  int dx, dy;
  
  image(mWaterMovie, 0, 0, WIDTH, HEIGHT);
  
  loadPixels();
  oldPix = pixels.clone();
  
  int halfWidth = WIDTH/2;
  int halfHeight = HEIGHT/2;

  for (int x=0; x<WIDTH; ++x) {
    for (int y=0; y<HEIGHT; ++y) {
      // coordinates of displaced pixel in this spot
      //dx = (((x-halfWidth)*(1024-bufSrc[x+1][y+1]))>>10)+halfWidth;
      //dy = (((y-halfHeight)*(1024-bufSrc[x+1][y+1]))>>10)+halfHeight;
      
      //alternative displacement with correct gradient
      dx = x + bufSrc[1+x-1][1+y] - bufSrc[1+x+1][1+y];
      dy = y + bufSrc[1+x][1+y-1] - bufSrc[1+x][1+y+1];

      //bounds check
      dx = max(0,min(WIDTH-1,dx));
      dy = max(0,min(HEIGHT-1,dy));
      
      
      if(showScreen == SCREEN_DISTANCE) {
        //alternative to show disturbed pixels
        pixels[y*width+x] = color(0,bufSrc[1+x][1+y],0);
        bufSrc[1+x][1+y]*=0.8;
      } else {
        pixels[y*width+x] = oldPix[dy*width+dx];
      }
    }
  }

  updatePixels();
}

private void kFilterWithThreshold(int[] a,int width, int height, int kernelType, int threshold) {
  int[] b = a.clone();
  float[][] kernel = medianKernel;
  
  switch(kernelType) {
    case MEDIAN:  kernel = medianKernel;  break;
  }

  for (int y = 1; y < height-1; y++) {
    for (int x = 1; x < width-1; x++) {
      float sum = 0;
      for (int ky = -1; ky <= 1; ky++) {
        for (int kx = -1; kx <= 1; kx++) {
          int pos = (y + ky)*width + (x + kx);
          float val = b[pos];
          sum += kernel[ky+1][kx+1] * val;
        }
      }
      if(sum >= threshold)
        a[y*width+x] = (int)sum;
      else
        a[y*width+x] = 0;
    }
  }
}

public void mousePressed() {
  if(drawPhaseBox) {
    phaseBox[0] = mouseX;
    phaseBox[1] = mouseY-208;
  }
  else {
    tuioBox[0] = mouseX;
    tuioBox[1] = mouseY-208;
  }
}

public void mouseDragged() {
  if(drawPhaseBox) {
    phaseBox[2] = mouseX;
    phaseBox[3] = mouseY-208;
  }
  else {
    tuioBox[2] = mouseX;
    tuioBox[3] = mouseY-208;
  }
}

public void mouseReleased() {
}

void keyReleased() {
  switch(key) {
    case SCREEN_MAIN:
        showScreen = SCREEN_MAIN;
        break;
    case SCREEN_OPTIONS:
        showScreen = SCREEN_OPTIONS;
        break;
    case SCREEN_DISTANCE:
        showScreen = SCREEN_DISTANCE;
        break;
    case SCREEN_DEPTH:
        showScreen = SCREEN_DEPTH;
        break;
    case SCREEN_DEPTH_PLANES:
        showScreen = SCREEN_DEPTH_PLANES;
        break;
    case SCREEN_CAM:
        showScreen = SCREEN_CAM;
        break;
    case 'c':
        if(drawCursor)
          drawCursor = false;
        else
          drawCursor = true;
        break;
    case 'b':
        if(drawBox)
          drawBox = false;
        else
          drawBox = true;
        break;
    case 'n':
        if(drawPhaseBox)
          drawPhaseBox = false;
        else
          drawPhaseBox = true;
        break;
    case 'i':
        loadConfig();
        break;
    case 'm':
        writeConfig();
        break;
    case 't':
        if(showTuioCaliPlane)
          showTuioCaliPlane = false;
        else
          showTuioCaliPlane = true;
        break;
        
        
  }
  
  // reset stateswitch
  oldWaitForChange = System.currentTimeMillis();
}

int sign = 1;

void keyPressed() {
  int[] tmp;
  int indexMaxPlane = 1;
  
  if(showTuioCaliPlane && !drawPhaseBox)
    indexMaxPlane = 2;
  if(drawPhaseBox)
    tmp = phasePlanes;
  else
    tmp = tuioPlanes;
  switch(java.lang.Character.toLowerCase(key)) {
    case 'q':
        tmp[0] += sign*1;
        break;
    case 'w':
        tmp[0] += sign*10;
        break;
    case 'e':
        tmp[0] += sign*100;
        break;
    case 'a':
        tmp[indexMaxPlane] += sign*1;
        break;
    case 's':
        tmp[indexMaxPlane] += sign*10;
        break;
    case 'd':
        tmp[indexMaxPlane] += sign*100;
        break;
    case 'x':
        sign = -1*sign;
        break;
  }
}

private void loadConfig() {
    BufferedReader br = null;
    try {
        br = new BufferedReader(new FileReader(sketchPath("config.txt")));
        tuioPlanes[0] = Integer.parseInt(br.readLine());
        tuioPlanes[1] = Integer.parseInt(br.readLine());
        tuioPlanes[2] = Integer.parseInt(br.readLine());
        phasePlanes[0] = Integer.parseInt(br.readLine());
        phasePlanes[1] = Integer.parseInt(br.readLine());
        tuioBox[0] = Integer.parseInt(br.readLine());
        tuioBox[1] = Integer.parseInt(br.readLine());
        tuioBox[2] = Integer.parseInt(br.readLine());
        tuioBox[3] = Integer.parseInt(br.readLine());
        phaseBox[0] = Integer.parseInt(br.readLine());
        phaseBox[1] = Integer.parseInt(br.readLine());
        phaseBox[2] = Integer.parseInt(br.readLine());
        phaseBox[3] = Integer.parseInt(br.readLine());
        br.close();
    }
    catch(Exception e)
    {
    } finally {
    }
}

private void writeConfig() {
  PrintWriter pw = null;
  try
  {
    Writer fw = new FileWriter( sketchPath("config.txt"));
    Writer bw = new BufferedWriter( fw );
    pw = new PrintWriter( bw );

    pw.println(tuioPlanes[0]);
    pw.println(tuioPlanes[1]);
    pw.println(tuioPlanes[2]);
    pw.println(phasePlanes[0]);
    pw.println(phasePlanes[1]);
    pw.println(tuioBox[0]);
    pw.println(tuioBox[1]);
    pw.println(tuioBox[2]);
    pw.println(tuioBox[3]);
    pw.println(phaseBox[0]);
    pw.println(phaseBox[1]);
    pw.println(phaseBox[2]);
    pw.println(phaseBox[3]);
  } catch ( IOException e ) {
      System.err.println( "Error creating file!" );
  }
  finally {
    if ( pw != null )
      pw.close();
  }
}
  
public boolean sketchFullScreen() {
  return ENABLEFULLSCREEN;
}

private int randomLim(int low, int high) {
    high++;
    return (int) (Math.random() * (high - low) + low);
}

// ***************** PHASE 2 MEMBERS ******************
void enterPhase2()
{
  calibrating = true;
  smoothingCursors = new ArrayList<PVector>();
  calibCursors = new ArrayList<PVector>();
  for(int i = 0; i < 10; ++i)
    calibCursors.add(new PVector(random(WIDTH), random(HEIGHT)));
  
  mZMod = 0;
  mFlow = 0;
  mFlowAccu = 0;
  mA = 0;
  mDepth = 0;
  mCameraPosition = new PVector(0, 0);
  
  mMaximumCalibTime = System.currentTimeMillis() + 10000;
  
  mBgMovie.loop();
  movieFrameReady = false;
  
  VORTEX_WIDTH = (int)(0.6*width);
  VORTEX_HEIGHT = (int)(0.6*height);
  
  initPhases();
  initParticleSystems();
  
  mVortex = new Vortex(this, VORTEX_SIZE, mPhases[0]);
  switchPhase(0);
  
  mCurrentPhase = 0;
  switchPhase(mCurrentPhase);
      
  hint(DISABLE_DEPTH_TEST);

  setChannelFade(0,true,0.1f);
  startTime = System.currentTimeMillis();

}
void exitPhase2()
{
  mBgMovie.stop();
}

void drawPhase2()
{
  kinect.update();
  boolean hit = calcCursorPos(kinect.depthMap(), 0);
  
  float mX = hit ? cursorX : WIDTH*0.5;
  float mY = hit ? cursorY : HEIGHT*0.5;
  if(calibrating)
  {
    if(hit)
    {
     calibCursors.add(new PVector(mX, mY));
     if(calibCursors.size() > 20)
       calibCursors.remove(0);
     
     PVector mean = new PVector(0,0);
     for(int i = 0; i < calibCursors.size(); ++i)
     {
       mean.add(calibCursors.get(i));
     }
     mean.div(calibCursors.size());
     
     float var = 0;
     for(int i = 0; i < calibCursors.size(); ++i)
     {
       PVector p = calibCursors.get(i).get();
       p.sub(mean);
       var += p.mag();
     }
     var /= calibCursors.size();
     
     System.out.println(String.valueOf(var) + "," + String.valueOf(mX) + "," + String.valueOf(mY));
     
     if(var < CALIB_THRESHOLD)
     {
       calibrating = false;
       cursorXorigin = (int)mean.x;
       cursorYorigin = (int)mean.y;
     }
     else
     {
       mX = WIDTH*0.5;
       mY = HEIGHT*0.5;
     }
     }
     
   }
   else
   {
     mX -= cursorXorigin;
     mY -= cursorYorigin;
     mX *= CURSOR_X_SCALE;
     mY *= CURSOR_Y_SCALE;
     mX += WIDTH*0.5;
     mY += HEIGHT*0.5;
     
     PVector cursor = new PVector(0,0);
     smoothingCursors.add(new PVector(mX, mY));
     if(smoothingCursors.size() > SMOOTHING_CURSOR_SIZE)
       smoothingCursors.remove(0);
     for(int i = 0; i < smoothingCursors.size(); ++i)
     {
       cursor.add(smoothingCursors.get(i));
     }
     cursor.div(smoothingCursors.size());
     
     mX = cursor.x;
     mY = cursor.y;
   }
   
  background(0);
  Phase currentPhase = mPhases[mCurrentPhase];
  
  mTarget = new PVector(width-mX-width/2, mY-height/2, mCameraPosition.z);
  mTarget.mult(2);
  
  PVector nearest = mCameraPosition.get(); nearest.z += 10;
  nearest = mVortex.getNearest(nearest);
  nearest.z = mCameraPosition.z;
  
  PVector targetVector = mTarget.get(); targetVector.sub(mCameraPosition);
  PVector distVector = nearest.get(); distVector.sub(mCameraPosition);
  PVector targetDist = mTarget.get(); targetDist.sub(nearest);
  
  float dist = distVector.mag();
  float tDist = targetDist.mag();
  
  boolean isInsideVortex = 0.5*dist < currentPhase.vortexRadius;
  boolean isNearVortex = 0.35*dist < currentPhase.vortexRadius;
  
  
  float acc = (1.5*currentPhase.vortexRadius) / dist - 0.5;
  acc = min(0.007, acc);
  if(mFlow < 0.6*AVG_SPEED)
    acc = max(0.1, acc);
  
  if(min(tDist, dist) <= 9*currentPhase.vortexRadius)
    mCameraPosition = interpolate(mCameraPosition, mTarget, 0.99);
  
  distVector.normalize();
  float adhesionScale = (currentPhase.vortexRadius-dist)/(6*currentPhase.vortexRadius);
  mCameraPosition = interpolate(mCameraPosition, nearest, 1.0-max(0.0, min(adhesionScale, 0.1)));
  
  mFlow += acc;
  mFlow = constrain(mFlow, MIN_SPEED, MAX_SPEED);
  mZMod += mFlow;
  mDepth += mFlow;
  
  if(mFlow > AVG_SPEED)
  {
    mFlowPhaseAccu += mFlow;
    mFlowAccu += mFlow;
  }
  
  if(mCurrentPhase < 4 && mFlowPhaseAccu > currentPhase.flowLength)
  {
    System.out.println(String.valueOf(mFlowPhaseAccu) + " > " + String.valueOf(currentPhase.flowLength));
    mCurrentPhase++;
    switchPhase(mCurrentPhase);
    mFlowPhaseAccu = 0;
  }
  
  
  PVector bg = new PVector(0, 0, max(255-mFlowAccu*BG_FACTOR, 0));
  lightSpecular(bg.x, bg.y, bg.z);
  directionalLight(128, 128, 128, 0, -1, -1);
  ambientLight(150, 150, 150);
  
  
  pushMatrix();
  //pos.add(delta);
  camera(mCameraPosition.x, mCameraPosition.y, mCameraPosition.z,
         mTarget.x, mTarget.y, VORTEX_DIST, 0, 1, 0);
       
  pushMatrix();
  
  shader(bgGradient);
  translate(0, 0, -80-mCameraPosition.z);
  noStroke();
  texturedCube();
  popMatrix();
  
         
  // update the tube that is placed at mDepth mod VORTEX_DIST
  pushMatrix();
  translate(0, 0, -mZMod);
  //mVortex.drawLines();
  lightSpecular(255, 255, 255);
  shader(texLight); 
  mVortex.drawTube();
  popMatrix();
  
  // now update Particles and stuff that are placed at mDepth
  translate(0, 0, -mDepth);
  //shader(light); 
  mAmoeba.update();
  mJellyfish.update();
  mMoreJellyfish.update();
  mStrobo.update();
  mBubbles.update();
  mSudden.update();
  
  mAmoeba.display();
  mJellyfish.display();
  mMoreJellyfish.display();
  mStrobo.display();
  mSudden.display();
  mBubbles.display();
  
  if(mZMod > VORTEX_LENGTH)
  {
    mZMod -= VORTEX_LENGTH;
    mVortex.advance();
  }
  popMatrix();
  
  image(mTargetImage, mX-73, mY-72);
  
  if(mShowInfo)
  {
    if(calibrating)
    {
      tint(255, 255);
      image(mInfo1, width/2-227, height/2-207);
    }
    else if(mInfoAlpha1 > 0)
    {
      mInfoAlpha1 -= 20;
      mInfoAlpha1 = max(0, mInfoAlpha1);
      tint(255, mInfoAlpha1);
      image(mInfo1, width/2-227, height/2-207);
    }
    else if(!mAllInfoShown && mInfoAlpha2 < 255)
    {
      mInfoAlpha2 += 20;
      mInfoAlpha2 = min(255, mInfoAlpha2);
      tint(255, mInfoAlpha2);
      image(mInfo2, width/2-225, height/2-225);
    }
    else if(mInfoFramesShown < 20)
    {
      mAllInfoShown = true;
      mInfoFramesShown++;
      tint(255, 255);
      image(mInfo2, width/2-225, height/2-225);
    }
    else if(mAllInfoShown && mInfoAlpha2 > 0)
    {
      mInfoAlpha2 -= 2;
      mInfoAlpha2 = max(0, mInfoAlpha2);
      tint(255, mInfoAlpha2);
      image(mInfo2, width/2-225, height/2-225);
    }
    else
    {
      mShowInfo = false;
    }
    
  }

  float normalizedFlow = (mFlow-MIN_SPEED)/(MAX_SPEED-MIN_SPEED);
  colorFade(normalizedFlow * COLOR_CHANGE_SPEED * (mCurrentPhase+0.5f));

  // sound
  float th = 1.0f / (channels.length+1);
  for (int i=1; i<channels.length; ++i)
    if (normalizedFlow > th*i)
      setChannelFade(i, true, 0.1f);
    else
      setChannelFade(i, false, 0.4f);

  long momTime = System.currentTimeMillis();
  fadeChannels(momTime-startTime);
  startTime = momTime;
}

// Called every time a new frame is available to read
void movieEvent(Movie m)
{
  m.read();
  movieFrameReady = true;
}

PVector interpolate(PVector left, PVector right, float lambda)
{
  PVector a = new PVector(left.x, left.y);
  a.mult(lambda);
  PVector b = new PVector(right.x, right.y);
  b.mult(1-lambda);
  a.add(b);
  return a;
}

void texturedCube() {
  beginShape(QUADS);
  
  texture(mBgMovie);
  
  
  float scale = 2.6;
  // Given one texture and six faces, we can easily set up the uv coordinates
  // such that four of the faces tile "perfectly" along either u or v, but the other
  // two faces cannot be so aligned.  This code tiles "along" u, "around" the X/Z faces
  // and fudges the Y faces - the Y faces are arbitrarily aligned such that a
  // rotation along the X axis will put the "top" of either texture at the "top"
  // of the screen, but is not otherwised aligned with the X/Z faces. (This
  // just affects what type of symmetry is required if you need seamless
  // tiling all the way around the cube)
  
  // +Z "front" face
  vertex(-scale*width, -scale*height,  scale*height, 0, 0);
  vertex( scale*width, -scale*height,  scale*height, 1, 0);
  vertex( scale*width,  scale*height,  scale*height, 1, 1);
  vertex(-scale*width,  scale*height,  scale*height, 0, 1);

  // -Z "back" face
  vertex( scale*width, -scale*height, -scale*height, 0, 0);
  vertex(-scale*width, -scale*height, -scale*height, 1, 0);
  vertex(-scale*width,  scale*height, -scale*height, 1, 1);
  vertex( scale*width,  scale*height, -scale*height, 0, 1);

  // +Y "bottom" face
  vertex(-scale*width,  scale*height,  scale*height, 0, 0);
  vertex( scale*width,  scale*height,  scale*height, 1, 0);
  vertex( scale*width,  scale*height, -scale*height, 1, 1);
  vertex(-scale*width,  scale*height, -scale*height, 0, 1);

  // -Y "top" face
  vertex(-scale*width, -scale*height, -scale*height, 0, 0);
  vertex( scale*width, -scale*height, -scale*height, 1, 0);
  vertex( scale*width, -scale*height,  scale*height, 1, 1);
  vertex(-scale*width, -scale*height,  scale*height, 0, 1);

  // +X "right" face
  vertex( scale*width, -scale*height,  scale*height, 0, 0);
  vertex( scale*width, -scale*height, -scale*height, 1, 0);
  vertex( scale*width,  scale*height, -scale*height, 1, 1);
  vertex( scale*width,  scale*height,  scale*height, 0, 1);

  // -X "left" face
  vertex(-scale*width, -scale*height, -scale*height, 0, 0);
  vertex(-scale*width, -scale*height,  scale*height, 1, 0);
  vertex(-scale*width,  scale*height,  scale*height, 1, 1);
  vertex(-scale*width,  scale*height, -scale*height, 0, 1);

  endShape();
}

void fadeChannels(long deltaT) {
  for(int i=0; i<channels.length; i++) {
    float fade = FADE_CONSTANT*fadeSpeed[i]*(deltaT/1000f);
    if(channelFade[i]) {
      if(1-channels[i].amplitude.getLastValue() > fade)
        channels[i].amplitude.setLastValue(channels[i].amplitude.getLastValue() + fade);
      else 
        channels[i].amplitude.setLastValue(1f);
    }    
    else {
      if(channels[i].amplitude.getLastValue() > fade)
        channels[i].amplitude.setLastValue(channels[i].amplitude.getLastValue() - fade);
      else 
        channels[i].amplitude.setLastValue(0f);
    }    
  }
}

void setChannelFade(int nr, boolean toFade, float fadeSpe) {
  channelFade[nr] = toFade;
  fadeSpeed[nr] = fadeSpe;
}

void stopSound() {
  for(Sampler s: channels) {
    s.amplitude.setLastValue(0f);  // mute channel
  }
}

