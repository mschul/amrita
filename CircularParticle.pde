class CircularParticle implements I_Particle
{ 
  float mAngle;
  float mVel;
  float mRadius;
  
  PVector mPos;
  PVector mPrevPos;
  
  CircularParticle(PVector p0)
  {
    reset(p0);
  }
  
  void update()
  {
    mPrevPos.x = mPos.x; mPrevPos.y = mPos.y; mPrevPos.z = mPos.z;
    mAngle += mVel;
    mPos.x = mRadius*cos(mAngle);
    mPos.y = mRadius*sin(mAngle);
  }
  void display(PImage tex, float mScale)
  {
    translate(mPos.x, mPos.y, mPos.z);
    
    // dir is delta from previous position to current, left is orthogonal to dir and vector to eye.
    PVector dir = mPrevPos.get(); dir.sub(mPos); dir.normalize();
    PVector left = mCameraPosition.get(); left.sub(mPos); left = left.cross(dir); left.normalize(); 
    
    // normalize and scale
    left.mult(mScale);
    dir.mult(mScale);
 
    // draw billboard
    beginShape();
    texture(tex);
    vertex(dir.x + left.x, dir.y + left.y, dir.z + left.z, 1, 1);
    vertex(dir.x - left.x, dir.y - left.y, dir.z - left.z, 0, 1);
    vertex(-dir.x - left.x, -dir.y - left.y, -dir.z - left.z, 0, 0);
    vertex(-dir.x + left.x, -dir.y + left.y, -dir.z + left.z, 1, 0);
    endShape();
  }
  boolean isDead()
  {
    return mPos.z < mDepth;
  }
  float z()
  {
    return mPos.z;
  }
  float dist(PVector pos)
  {
    return mPos.dist(pos);
  }
  void reset(PVector p0)
  {
    mRadius = random(120, 400);
    mAngle = random(0, 2*PI);
    
    mPos = p0.get();
    mPos.x = mRadius*cos(mAngle);
    mPos.y = mRadius*sin(mAngle);
    mPrevPos = mPos.get();
    
    mVel = random(0.001, 0.05);
  }
}
