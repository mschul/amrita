#ifdef GL_ES
precision mediump float;
precision mediump int;
#endif

uniform mat4 transform;
uniform mat4 texMatrix;

varying vec4 vertTexCoord;
uniform int lightCount;
uniform vec4 lightPosition[8];
uniform vec3 lightNormal[8];
uniform vec3 lightAmbient[8];
uniform vec3 lightDiffuse[8];
uniform vec3 lightSpecular[8];
uniform vec3 lightFalloff[8];
uniform vec2 lightSpot[8];

attribute vec4 ambient;
attribute vec2 texCoord;

attribute vec4 vertex;
attribute vec4 color;
varying vec4 vertColor;
void main()                             
{
	gl_Position = transform * vertex;   
    vertColor = vec4(lightSpecular[0], 1.0);
	vertTexCoord = texMatrix * vec4(texCoord, 1.0, 1.0);
}   