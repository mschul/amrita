import shapes3d.utils.*;
import shapes3d.animation.*;
import shapes3d.*;

class Radius implements I_RadiusGen
{
  float mRadius;
  Radius(float radius)
  {
    mRadius = radius;
  }
  
  float radius(float t)
  {
    return mRadius;
  }
  
  boolean hasConstantRadius()
  {
    return true;
  }
  
  Object getController()
  {
    return new Object();
  }
}
class Path implements I_PathGen
{
  int mSize;
  P_BezierSpline mSpline;
  PVector mBack;
  
  Path(int size)
  {
    mSize = size;
    
    PVector[] path = new PVector[size];
    for(int i = 0; i < size; ++i)
    {
      path[i] = getVortexPosition();
    }
    mBack = path[size-1].get();
    mSpline = new P_BezierSpline(path);
  }
  
  Path(int size, PVector first)
  {
    mSize = size;
    
    PVector[] path = new PVector[size];
    path[0] = first.get();
    for(int i = 1; i < size; ++i)
    {
      path[i] = getVortexPosition();
    }
    mBack = path[size-1].get();
    mSpline = new P_BezierSpline(path);
  }
  
  PVector getVortexPosition()
  {
    return new PVector(random(VORTEX_WIDTH/3)-VORTEX_WIDTH/6, random(VORTEX_HEIGHT/3)-VORTEX_HEIGHT/6);
  }
  
  float x(float t)
  {
    return mSpline.point(t).x;
  }
  
  float y(float t)
  {
    return mSpline.point(t).y;
  }
  
  float z(float t)
  {
    float lambda = (mSize-1) * t;
    return VORTEX_DIST*(lambda);
  }
  
  PVector back()
  {
    return mBack;
  }
  PVector at(float t)
  {
    return new PVector(x(t), y(t), z(t));
  }
}

class VortexSegment
{
  ArrayList<PathTube> mTubes;
  Path mPath;
  int mSize;
  
  VortexSegment(int size)
  {
    mPath = new Path(size);
    mSize = size;
    mTubes = new ArrayList<PathTube>();
  }
  VortexSegment(int size, VortexSegment other)
  {
    mPath = new Path(size, other.back());
    mSize = size;
    mTubes = new ArrayList<PathTube>();
  }
  
  void display()
  {
    for(PathTube tube : mTubes)
      tube.draw();
  }
  
  PVector back()
  {
    return mPath.back();
  }
  
  PVector at(float t)
  {
    return mPath.at(t);
  }
  
  void addTube(PApplet app, float radius, String texture)
  {
    Radius r = new Radius(radius);
    PathTube p = new PathTube(app, mPath, r, mSize*8-2, 32, false);
    p.setTexture(texture,5,32);
    p.drawMode(S3D.TEXTURE);
    p.visible(false, S3D.BOTH_CAP);
    mTubes.add(p);
  }
}

class Vortex
{
  PApplet mApp;
  int mSize;
  
  float mVortexRadius;
  int mVortexLayerCount;
  String[] mVortexTextures;
  float[] mVortexRadiusFactors;
  
  VortexSegment[] mVortex;
  
  Vortex(PApplet app, int size, Phase p)
  {
    mApp = app;
    mSize = size;
    
    setParams(p);
    
    mVortex = new VortexSegment[2];
    mVortex[0] = new VortexSegment(size);
    
    for(int i = 0; i < mVortexLayerCount; ++i)
      mVortex[0].addTube(mApp, mVortexRadiusFactors[i]*mVortexRadius, mVortexTextures[i]);
    
    mVortex[1] = new VortexSegment(size, mVortex[0]);
    for(int i = 0; i < mVortexLayerCount; ++i)
      mVortex[1].addTube(mApp, mVortexRadiusFactors[i]*mVortexRadius, mVortexTextures[i]);
  }

  void setParams(Phase p)
  {
    mVortexRadius = p.vortexRadius;
    mVortexLayerCount = p.vortexLayerCount;
    mVortexTextures = p.vortexTextures;
    mVortexRadiusFactors = p.vortexRadiusFactors;
  }
  
  void advance()
  {
    mVortex[0] = mVortex[1];
    mVortex[1] = new VortexSegment(mSize, mVortex[0]);
    for(int i = 0; i < mVortexLayerCount; ++i)
      mVortex[1].addTube(mApp, mVortexRadiusFactors[i]*mVortexRadius, mVortexTextures[i]);
  }
  
  PVector getNearest(PVector pos)
  {
    float t = (float)(mZMod) / VORTEX_LENGTH;
    return mVortex[0].at(t).get();
  }

  void drawTube()
  {
    pushMatrix();
    fill(150);
    mVortex[0].display(); 
    translate(0, 0, VORTEX_LENGTH);
    mVortex[1].display();
    popMatrix();
  }
  
  PVector getAt(float z)
  {
    float t = z / VORTEX_DIST;
    t /= (mSize-1);
    return mVortex[0].at(t);
  }
  PVector getSecondAt(float z)
  {
    float t = z / VORTEX_DIST;
    t /= (mSize-1);
    return mVortex[1].at(t);
  }
}
