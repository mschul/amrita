interface I_Particle
{
  void reset(PVector p0);
  void update();
  void display(PImage tex, float scale);
  float z();
  float dist(PVector pos);
  boolean isDead();
}


class ParticleSystem
{
  boolean mCollide;
  ArrayList<I_Particle> mParticles;
  ArrayList<PImage> mParticleAnim;
  float mAnimFrame;
  float mAnimSpeed;
  String mAnimName;
  boolean mActive;
  float mScale;
  int mType;
  int mCount;
  
  ParticleSystem(int particleCount, String imagePrefix, int animLength, float animSpeed,
                 boolean collide, int type, float scale)
  {
    mCount = particleCount;
    mType = type;
    mAnimName = imagePrefix;
    mAnimSpeed = animSpeed;
    mScale = scale;
    mParticles = new ArrayList<I_Particle>();
    
    mParticleAnim = new ArrayList<PImage>();
    for(int i = 0; i < animLength; ++i)
    {
      String filename = imagePrefix + nf(i, 5) + ".png";
      mParticleAnim.add(loadImage(filename));
    }
    mAnimFrame = 0;
    mCollide = collide;
    mActive = false;
  }

  void activate()
  {
    if(!mActive)
    {
      //PVector origin = new PVector(0, 0, mDepth + 2*VORTEX_DIST);
      mActive = true;
      System.out.println("creating particles " + mAnimName);
      for(int i = 0; i < mCount; ++i)
      {
        PVector p0 = mVortex.getSecondAt(random(0, VORTEX_LENGTH));
        p0.z += mDepth + VORTEX_LENGTH;
        p0.z += mZMod;
        if(mType == BUBBLE) mParticles.add(new BubbleParticle(p0));
        if(mType == CIRCULAR) mParticles.add(new CircularParticle(p0));
        if(mType == FLOATY) mParticles.add(new FloatyParticle(p0));
      }
    }
  }
  void deactivate()
  {
    if(mActive)
    {
      System.out.println("deactivating particles");
      //PVector origin = new PVector(0, 0, mDepth + 2*VORTEX_DIST);
      mActive = false;
    }
  }

  void update()
  {
    PVector pos = mCameraPosition.get();
    pos.z = mDepth;
    //PVector origin = new PVector(0, 0, mDepth + 2*VORTEX_DIST);
    for (int i = mParticles.size()-1; i >= 0; i--)
    {
      I_Particle p = mParticles.get(i);
      p.update();
      if (p.isDead() && mActive)
      {
        PVector p0 = mVortex.getSecondAt(random(0, VORTEX_LENGTH));
        p0.z += mDepth;
        p0.z += mZMod;
        p.reset(p0);
      }
      
      if(mCollide
      && p.z() < mDepth+2*mScale+10
      && p.dist(pos) < 2*mScale)
      {
          mFlow *= 0.5f;
          mFlow = max(MIN_SPEED, mFlow);
      }
    }
    
    mAnimFrame += mAnimSpeed;
    if((int)mAnimFrame >= mParticleAnim.size()) mAnimFrame = 0;
  }
  
  void display()
  {
    noStroke();
    shininess(20.0);
    fill(255,255,255,200);
    for (I_Particle p : mParticles)
    {
      pushMatrix();
      p.display(mParticleAnim.get((int)mAnimFrame), mScale);
      popMatrix();
    }
  }
}
