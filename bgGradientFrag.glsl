#ifdef GL_ES
precision mediump float;
precision mediump int;
#endif                             

uniform sampler2D texture;
varying vec4 vertTexCoord;

uniform vec2 resolution;

varying vec4 vertColor;
void main()                             
{
	vec4 color = vertColor;
	if(color.b < 0.333)
	{
		color = (1.0-3.0*color.b) * texture2D(texture, vertTexCoord.st) + vertColor;
	}
	
    vec2 position = ( gl_FragCoord.xy / resolution.xy );
	float dist = length(position - vec2(0.5, 0.5));
    gl_FragColor = mix(color, vec4(0.0,0.0,0.0,1.0), dist);
}  