class FloatyParticle implements I_Particle
{ 
  PVector mPos;
  PVector mPrevPos;
  
  PVector mVel;
  PVector mAcc;
  float mLife;
  
  FloatyParticle(PVector p0)
  {
    reset(p0);
  }
  
  void update()
  {
    mPrevPos.x = mPos.x; mPrevPos.y = mPos.y; mPrevPos.z = mPos.z;
    mVel.add(mAcc);
    mPos.add(mVel);
  }
  void display(PImage tex, float mScale)
  {
    translate(mPos.x, mPos.y, mPos.z);
    
    // dir is delta from previous position to current, left is orthogonal to dir and vector to eye.
    PVector dir = mPrevPos.get(); dir.sub(mPos); dir.normalize();
    PVector left = mCameraPosition.get(); left.sub(mPos); left = left.cross(dir); left.normalize(); 
    
    // normalize and scale
    left.mult(mScale);
    dir.mult(mScale);
 
    // draw billboard
    beginShape();
    texture(tex);
    vertex(dir.x + left.x, dir.y + left.y, dir.z + left.z, 1, 1);
    vertex(dir.x - left.x, dir.y - left.y, dir.z - left.z, 0, 1);
    vertex(-dir.x - left.x, -dir.y - left.y, -dir.z - left.z, 0, 0);
    vertex(-dir.x + left.x, -dir.y + left.y, -dir.z + left.z, 1, 0);
    endShape();
  }
  boolean isDead()
  {
    return mPos.z < mDepth;
  }
  float z()
  {
    return mPos.z;
  }
  float dist(PVector pos)
  {
    return mPos.dist(pos);
  }
  void reset(PVector p0)
  {
    mPos = new PVector(random(-width/4, width/4), random(-height/4, height/4), p0.z + random(100));
    mPrevPos = mPos.get();
    
    mVel = new PVector(random(-1.f, 1.f), random(-1.f, 1.f), random(-1.f, 1.f));
    mPos.add(mVel);
    
    mAcc = new PVector(0, 0);
  }
}
