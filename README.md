so kalibrieren erklärt:

man zeichnet 2 Quader (AABs) in den Raum um verschiedene Dinge triggern zu lassen. Dafür erstmal mit 5 auf den calibration screen wechseln.
Unten stehen zwei Intervalle: Phase Change und TUIO. ein bisschen mit e und d rumspielen um zu sehen wie sich die min und max Werte ändern, mit x kann man das Vorzeichen der Veränderung ändern. Intervall so lange vergrößern (also min-Wert kleiner max-Wert größer) bis Tiefenbild sichtbar.

Standardmäßig sollte rechts oben Draw Phase Change Box stehen. Die triggert bei Aktivität den Beginn des Spiels. Sollte sich also auf Brusthöhe des Spielers befinden und vergleichsweise klein sein, von LINKS OBEN NACH RECHTS UNTEN ein Rechteck ziehen.

Nun mit n die Box wechseln. jetzt Prozess für TUIO Box wiederholen. Hier gibts noch die Tuio Calib Plane, man hat also 3 Werte (sowas wie min, mid und max). Erst die ersten beiden Werte festlegen, dass is dann wie der User tatsächlich steuert. Das Intervall muss natürlich den Arm in alle Richtungen ausgestreckt noch enthalten, aber kann auch deutlich größer gewählt werden. Man sollte aber beachten, dass Leute die vorne Vorbeilaufen sonst den Cursor fangen. Der dritte Wert ist für die Kalibrierung uns MUSS zwischen den ersten beiden liegen. Um ihn anzusteuern t drücken. Und ja die Tastenkombinationen sind behindert aber wir ham uns da was bei gedacht.

Das Ganze nicht vergessen mit m zu speichern (später mit i laden) und dann auf die Wasseroberfläche mit 1 wechseln und in die Phase Change Box reingehen.

Nach dem Start des Spiels wird der User gebeten die Hand nach vorne zu strecken. Hier mittel ich dann über die Handposition und nehm, sobald das konvergiert den Mittelwert als Nullpunkt der Steuerung. (Damit sich auch Spieler der Handballnationalmannschaft nicht bücken müssen)


# screens #
1: main game

2: screen options (actually unused)

3: screen distance

4: screen depth

5: screen depth planes (CALIBRATION IS HERE)

6: screen cam


# calibration usage #
c: draw cursor

b: draw box

n: switch calibration box

i: load config

m: write config

t: show tuio cali plane

x: choose inc/dec sign

q: plane0 inc/dec 1

w: plane0 inc/dec 10

e: plane0 inc/dec 100

a: plane1 inc/dec 1

s: plane1 inc/dec 10

d: plane1 inc/dec 100